<?php

namespace Libriciel\Login;

/**
 * La seule utilité de cette classe est d'éviter une erreur lors du
 * composer install de cette library
 */
class Lib
{
    /**
     * @return string
     */
    public static function version(): string
    {
        return '1.0.0';
    }
}
