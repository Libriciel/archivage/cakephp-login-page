# Cakephp3 Libriciel Login Page

## Installation

### Composer

Ajoutez le repository ainsi que le script d'install à votre __composer.json__

```json
{
    "repositories": {
        "libriciel/cakephp-login-page": {
            "type": "git",
            "url": "git@gitlab.libriciel.fr:CakePHP/cakephp-login-page.git"
        }
    },
    "scripts": {
        "post-update-cmd": [
            "vendor/bin/libriciel-login-assets-installer webroot/"
        ],
    }
}
```

__NOTE:__ Le script est inutile si vous fournissez le css, les fonts et les images (voir configuration).

Lancez la commande `composer require libriciel/cakephp-login-page`

### Ajouter le plugin

Dans votre projet, dans `src/Application.php`, dans la fonction __Application::bootstrap()__:

```php
$this->addPlugin('Libriciel/Login');
```

## Utilisation

Dans l'action `UsersController::login()`:
```php
$this->viewBuilder()
    ->setLayout('Libriciel/Login.login')
    ->setTemplate('Libriciel/Login.login');
```

Dans l'action `UsersController::forgottenPassword()`:
```php
$this->viewBuilder()
    ->setLayout('Libriciel/Login.login')
    ->setTemplate('Libriciel/Login.forgottenPassword');
```


## Configuration

Liste des configurations possibles:   
[app_default.php](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page/blob/master/config/app_default.php)

### Exemple de configuration:

```php
return [
    /**
     * Configuration de la page de login
     */
    'Libriciel' => [
        'login' => [
            'css' => [
                'bootstrap.min.css',
                'font-awesome.min.css',
                '/libriciel-login/login.css',
                'libriciel_login_custom.css'
            ],
            'favicon' => [
                'filename' => 'favicon.png',
                'url' => '/favicon.png',
            ],
            'logo-header' => [
                'image' => 'Asalae_white_36px.png',
                'params' => [
                    'alt' => 'aslae',
                ]
            ],
            'logo-app' => [
                'image' => 'asalae.svg',
                'params' => [
                    'alt' => 'Logo asalae',
                    'class' => 'row center-block main-logo',
                ]
            ],
            'input' => [
                'username' => 'login',
                'password' => 'password',
                'displayPassword' => true,
                'email => 'username',
            ],
            'forgotten-password-url' => '/users/forgotten-password',
            'forgotten-password-label' => "Saisissez votre nom d'utilisateur",
            'forgotten-password-placeholder' => "Nom d'utilisateur",
            'footer' => [
                'softwares' => [
                    'asalae' => [
                        'image' => 'Asalae_white_24px.png',
                    ],
                ],
                'message' => "asalae ".ASALAE_VERSION_LONG."<span class=\"hidden-xs\"> - © 2010-".date('Y')." Libriciel SCOP</span>",
            ]
        ]
    ]
];
```