<?php
/**
 * @var Cake\View\View $this
 */
use Cake\Core\Configure;
use Cake\Utility\Hash;

$force = [];

// gestion Flash
$session = $this->getRequest()->getSession();
$flash = $session->read('Flash.flash');
if ($flash) {
    if (Hash::get($flash, '0.element') === 'flash/success') {
        $force['login-success-message'] = Hash::get($flash, '0.message');
    } else {
        $force['login-show-error'] = Hash::get($flash, '0.message');
    }
    $session->delete('Flash.flash.0');
}

$logoClient = Configure::read('Libriciel.login.logo-client');
if ($logoClient && !empty($logoClient['image'])) {
    $style = $logoClient['style'];
    $backgroundColor = trim(substr($style, strpos($style, ':') +1));
    $force += [
        'visual-configuration' => json_encode(
            [
                'showCustom' => true,
                'showInformation' => true,
                'colorBackground' => $backgroundColor,
                'imgData' => $logoClient['image'],
            ] + ($logoClient['addons'] ?? [])
        ),
    ];
}

echo $this->Html->tag(
    'ls-lib-login',
    '',
    $force + Configure::read('Libriciel.login.lib-login', [])
);
