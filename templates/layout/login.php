<?php
/**
 * @var Cake\View\View $this
 */
use Cake\Core\Configure;
use Cake\I18n\I18n;

/**
 * Structure HTML
 */
?>
<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', I18n::getLocale())?>">
<head>
    <?=$this->Html->charset()?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$this->fetch('title')?></title>
    <?=$this->Html->meta('icon')?>

    <?=$this->Html->meta(
        Configure::read('Libriciel.login.favicon.filename', 'favicon.png'),
        Configure::read('Libriciel.login.favicon.url', '/favicon.png'),
        ['type' => 'icon']
    )?>

    <?=$this->Html->script(
        Configure::read('Libriciel.login.js', [])
    )?>

    <?=$this->Html->css(
        Configure::read('Libriciel.login.css', [
            '/libriciel-login/bootstrap/bootstrap.min.css',
            '/libriciel-login/forkawesome/css/fork-awesome.css',
            '/libriciel-login/login.css',
        ])
    )?>

</head>
<body>

<div id="loading-screen">
    <noscript>
        <?=Configure::read(
            'Libriciel.login.message.nojs',
            __("Cette application nécéssite l'utilisation de javascript")
        )?>
    </noscript>
</div>

<?=$this->fetch('content')?>

<!-- Footer Libriciel SCOP -->
<?=$this->Html->tag(
    'ls-lib-footer',
    '',
    Configure::read('Libriciel.login.lib-footer', []) + [
        'class' => 'ls-login-footer ls-footer-fixed-bottom',
        'application_name' => Configure::read('App.version'),
    ]
)?>

<?=$this->Html->script(
    Configure::read('Libriciel.login.footer_js', [
        '/libriciel-login/ls-elements.js',
    ])
)?>
<script>
    document.getElementById('loading-screen').className = 'loaded';
</script>
</body>
</html>