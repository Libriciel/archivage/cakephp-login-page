<?php
return [
    /**
     * Configuration de la page de login
     */
    'Libriciel' => [
        'login' => [
            'favicon' => [
                'filename' => 'favicon.png',
                'url' => '/favicon.png',
            ],
            'js' => [
                'libriciel-login/bootstrap/js/bootstrap.min.js',
            ],
            'css' => [
                'libriciel-login/bootstrap/css/bootstrap.min.css',
                'libriciel-login/login.css',
            ],
            'nojs' => __("Cette application nécéssite l'utilisation de javascript"),
            'logo-header' => [
                'image' => 'mylogo-white.png',
                'params' => [
                    'alt' => 'logo',
                ]
            ],
            'logo-client' => [
                'style' => 'background: #158ed5',
                'image' => 'mylogoclient.png',
                'params' => [
                    'alt' => 'logo-client'
                ]
            ],
            'logo-app' => [
                'image' => 'mylogo.png',
                'params' => [
                    'alt' => 'Logo',
                    'class' => 'row center-block main-logo',
                ]
            ],
            'input' => [
                'username' => 'username',
                'password' => 'password',
                'displayPassword' => true,
                'email' => 'email',
            ],
            'forgotten-password-url' => '/users/forgotten-password',
            'forgotten-password-label' => "Saisissez votre identifiant de connexion ",
            'forgotten-password-placeholder' => "Identifiant de connexion ",
            'footer' => [
                'softwares' => [
                    'asalae' => [
                        'title' => 'asalae',
                        'href' => 'https://www.libriciel.fr/asalae',
                        'image' => '/libriciel-login/img/reduced_asalae.png',
                        'alt' => 'asalae',
                    ],
                    'i-delibRE' => [
                        'title' => 'i-delibRE',
                        'href' => 'https://www.libriciel.fr/i-delibre',
                        'image' => '/libriciel-login/img/i-delibRE_white_h24px.png',
                        'alt' => 'i-delibRE',
                        'style' => 'opacity: 0.5',
                    ],
                    'i-Parapheur' => [
                        'title' => 'i-Parapheur',
                        'href' => 'https://www.libriciel.fr/i-parapheur',
                        'image' => '/libriciel-login/img/reduced_iParapheur.png',
                        'alt' => 'i-Parapheur',
                    ],
                    'Pastell' => [
                        'title' => 'Pastell',
                        'href' => 'https://www.libriciel.fr/pastell',
                        'image' => '/libriciel-login/img/reduced_Pastell.png',
                        'alt' => 'Pastell',
                    ],
                    'S2low' => [
                        'title' => 'S²LOW',
                        'href' => 'https://www.libriciel.fr/s2low',
                        'image' => '/libriciel-login/img/reduced_S2low.png',
                        'alt' => 'S²LOW',
                    ],
                    'web-DPO' => [
                        'title' => 'web-DPO',
                        'href' => 'https://www.libriciel.fr/web-dpo',
                        'image' => '/libriciel-login/img/reduced_WebCil.png',
                        'alt' => 'web-DPO',
                    ],
                    'web-delib' => [
                        'title' => 'web-delib',
                        'href' => 'https://www.libriciel.fr/web-delib',
                        'image' => '/libriciel-login/img/reduced_WebDelib.png',
                        'alt' => 'web-delib',
                    ],
                    'web-GFC' => [
                        'title' => 'web-GFC',
                        'href' => 'https://www.libriciel.fr/web-gfc',
                        'image' => '/libriciel-login/img/reduced_WebGFC.png',
                        'alt' => 'web-GFC',
                    ],
                ],
                'message' => "Cakephp-login-page v1.0.0<span class=\"hidden-xs\"> - © 2019 Libriciel SCOP</span>",
            ]
        ]
    ]
];
